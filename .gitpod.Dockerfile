FROM gitpod/workspace-full

USER gitpod
RUN sudo add-apt-repository ppa:ondrej/php && \
    sudo apt-get update && \
    sudo apt-get install -y php7.4 php7.4-tidy php7.4-bcmath php7.4-curl php7.4-xml php7.4-mbstring php7.4-intl php7.4-mysql && \
    sudo rm -rf /var/lib/apt/lists/*
